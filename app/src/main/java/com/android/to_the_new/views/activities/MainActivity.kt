package com.android.to_the_new.views.activities

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.app.TimePickerDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.android.to_the_new.R
import com.android.to_the_new.di.modules.AppViewModelFactory
import com.android.to_the_new.models.LocationData
import com.android.to_the_new.models.TripInfo
import com.android.to_the_new.service.LocationService
import com.android.to_the_new.utils.*
import com.android.to_the_new.viewModels.MainActivityViewModel
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.create_trip_dialog.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


class MainActivity : AppCompatActivity() {


    @Inject
    lateinit var sessionManager: SessionManager

    @Inject
    lateinit var appViewModelFactory: AppViewModelFactory

    val mainActivityViewModel by lazy {
        ViewModelProvider(this, appViewModelFactory)[MainActivityViewModel::class.java]
    }

    private val LOCATION_REQUEST_CODE = 1000

    private var myCalendar: Calendar? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        setContentView(R.layout.activity_main)

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ),
                LOCATION_REQUEST_CODE
            )

        } else {
            init()
        }
    }

    private fun init() {
       this.checkProviderEnabled()
        myCalendar = Calendar.getInstance()
        handleTripButtonVisibility(createTrip = View.VISIBLE, onGoingTrip = View.VISIBLE)
        bt_mainActivity_create_trip.setOnClickListener { showCreateRoomDialog() }
        bt_mainActivity_end_trip.setOnClickListener { endTrip() }
    }

    private fun handleTripButtonVisibility(
        createTrip: Int = View.GONE,
        endTrip: Int = View.GONE,
        onGoingTrip: Int = View.GONE,
        progressBarTrip: Int = View.GONE,
        tripView: Int = View.GONE
    ) {
        bt_mainActivity_create_trip.visibility = createTrip
        bt_mainActivity_end_trip.visibility = endTrip
        layout_mainActivity_onGoingTrip.visibility = onGoingTrip
        progressBar_mainActivity.visibility = progressBarTrip
        layout_mainActivity_tripView.visibility = tripView

        if (progressBar_mainActivity.isVisible)
            tv_mainActivity_OnGoingTrip.text = getString(R.string.your_trip_ongoing)
        else tv_mainActivity_OnGoingTrip.text = getString(R.string.create_new_trip)


    }

    private fun showCreateRoomDialog() {
        val dialog = Dialog(this)
        dialog.apply {
            setContentView(R.layout.create_trip_dialog)
            setCancelable(true)
            tv_start_trip_time.setOnClickListener { showDatePickerDialog(tv_start_trip_time) }
            tv_end_trip_time.setOnClickListener { showDatePickerDialog(tv_end_trip_time) }

            bt_go.setOnClickListener {
                mainActivityViewModel.deleteLocation()
                val startTime = getStringToDate(tv_start_trip_time.text.toString())
                val endTime = getStringToDate(tv_end_trip_time.text.toString())

                if (startTime < endTime) {
                    handleTripButtonVisibility(
                        endTrip = View.VISIBLE,
                        onGoingTrip = View.VISIBLE,
                        progressBarTrip = View.VISIBLE
                    )
                    sessionManager.tripInfo = TripInfo(startTime = startTime, endTime = endTime)
                    Toast.makeText(
                        this@MainActivity,
                        "trip crated successfully",
                        Toast.LENGTH_SHORT
                    ).show()
                    startService(Intent(this@MainActivity, LocationService::class.java))
                    LocalBroadcastManager.getInstance(this@MainActivity)
                        .registerReceiver(broadcastReceiver, IntentFilter("Location"))
                    dismiss()
                } else {
                    Toast.makeText(
                        this@MainActivity,
                        "Your end trip time less then start time.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
            window?.setBackgroundDrawableResource(android.R.color.transparent)
            show()
        }
    }

    @SuppressLint("SetTextI18n")
    fun showDatePickerDialog(view: TextView) {
        myCalendar?.let { myCalendar ->
            myCalendar.timeInMillis = System.currentTimeMillis()
            val tpd = TimePickerDialog(
                this, R.style.PickerDialogCustom,
                { _, hourOfDay, minute ->

                    myCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                    myCalendar.set(Calendar.MINUTE, minute)
                    view.text = getDateFormat(myCalendar.time)
                    Log.e("date", getDateFormat(myCalendar.time))
                }, myCalendar.get(Calendar.HOUR_OF_DAY), myCalendar.get(Calendar.MINUTE), false
            )
            tpd.show()
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            LOCATION_REQUEST_CODE -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("Location --", "startService")
                }
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        endTrip()
    }


    private val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            val bundle = intent.extras
            val wayLatitude = bundle?.getDouble("wayLatitude")
            val wayLongitude = bundle?.getDouble("wayLongitude")
            val wayAccuracy = bundle?.getFloat("wayAccuracy")


            if (sessionManager.tripInfo?.endTime!! > System.currentTimeMillis()) {
                val locationData = LocationData(
                    latitude = wayLatitude ?: 0.0,
                    longitude = wayLongitude ?: 0.0,
                    timestamp = System.currentTimeMillis(),
                    accuracy = wayAccuracy ?: 0F,
                )
                mainActivityViewModel.addLocation(locationData)
                Log.d("locations_012", "$wayLatitude : $wayLongitude : $wayAccuracy")
            } else {
                Toast.makeText(
                    this@MainActivity,
                    "Your trip end now.",
                    Toast.LENGTH_SHORT
                ).show()
                endTrip()
            }
        }
    }

    private fun endTrip() {
        handleTripButtonVisibility(createTrip = View.VISIBLE, tripView = View.VISIBLE)
        stopService(Intent(this, LocationService::class.java))
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
        CoroutineScope(Dispatchers.IO).launch {
            val jsonObject = JSONObject()
            try {
                val jsonArray = JSONArray()
                mainActivityViewModel.getLocation().forEach {
                    val jsonArrayObject = JSONObject()
                    jsonArrayObject.put("latitude", "${it.latitude}")
                    jsonArrayObject.put("longitude", it.longitude)
                    jsonArrayObject.put("timestamp", getLongToDate(it.timestamp))
                    jsonArrayObject.put("accuracy", it.accuracy)
                    jsonArray.put(jsonArrayObject)
                }

                jsonObject.put("trip_id", sessionManager.tripInfo?.tripId)
                jsonObject.put(
                    "start_time",
                    getLongToDate(sessionManager.tripInfo?.startTime ?: System.currentTimeMillis())
                )
                jsonObject.put(
                    "end_time",
                    getLongToDate(sessionManager.tripInfo?.startTime ?: System.currentTimeMillis())
                )
                jsonObject.put("locations", jsonArray)
            } catch (e: JSONException) {
            }
            //  Log.e("location_end", jsonObject.toString(jsonObject.length()))

            runOnUiThread {
                tv_mainActivity_trip.text = jsonObject.toString(jsonObject.length())
            }
        }
    }





}