package com.android.to_the_new.application

import android.app.Activity
import android.app.Application
import android.app.Service
import android.content.Context
import androidx.fragment.app.Fragment
import com.android.to_the_new.di.components.AppComponent
import com.android.to_the_new.di.components.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.HasServiceInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject


class BaseApplication : Application(), HasActivityInjector, HasSupportFragmentInjector ,
    HasServiceInjector {

    companion object {

        lateinit var myApplication: BaseApplication

        fun applicationContext(): Context {
            return myApplication.applicationContext
        }
    }

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var serviceInjector: DispatchingAndroidInjector<Service>

    override fun activityInjector(): AndroidInjector<Activity> = activityInjector
    override fun supportFragmentInjector(): AndroidInjector<Fragment> = fragmentInjector
    override fun serviceInjector(): AndroidInjector<Service> = serviceInjector
    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        myApplication = this
        appComponent = DaggerAppComponent.builder()
            .application(this)
            .build()
        appComponent.inject(this)
    }

}