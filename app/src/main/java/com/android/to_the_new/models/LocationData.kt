package com.android.to_the_new.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "location")
data class LocationData(
    @PrimaryKey(autoGenerate = true)
    var id: Int=0,
    var latitude: Double=0.0,
    var longitude: Double =0.0,
    var timestamp: Long=0L,
    var accuracy: Float=0.0F
)