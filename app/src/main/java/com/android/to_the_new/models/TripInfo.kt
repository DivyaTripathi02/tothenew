package com.android.to_the_new.models

data class TripInfo(
    val tripId: Int = 1,
    val startTime: Long,
    val endTime: Long
)
