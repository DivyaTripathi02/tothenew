package com.android.to_the_new.viewModels

import androidx.lifecycle.ViewModel
import com.android.to_the_new.models.LocationData
import com.android.to_the_new.repository.LocationRepository
import javax.inject.Inject

class MainActivityViewModel @Inject constructor(
    private val locationRepository: LocationRepository
) : ViewModel() {

    fun addLocation(locationData: LocationData) = locationRepository.addLocation(locationData)
    suspend fun getLocation(): List<LocationData> = locationRepository.getLocation()
    fun deleteLocation() = locationRepository.deleteLocation()
}