package com.android.to_the_new.di.modules

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.util.Base64
import com.android.to_the_new.utils.AppConstants
import dagger.Module
import dagger.Provides
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class UtilsModules {

    @Singleton
    @Provides
    fun provideContext(application: Application): Context {
        return application.applicationContext
    }

    @Singleton
    @Provides
    fun providesAppPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences(
            AppConstants.SHARED_PREF_NAME,
            Context.MODE_PRIVATE
        )
    }

}