package com.android.to_the_new.di.modules

import com.android.to_the_new.service.LocationService
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ServiceModule {

    @ContributesAndroidInjector
    internal abstract fun contributeLocationServices(): LocationService
}