package com.android.to_the_new.di.modules

import com.android.to_the_new.views.activities.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule{


    @ContributesAndroidInjector
    internal abstract fun contributeMainActivity(): MainActivity

}