package com.android.to_the_new.di.components

import android.app.Application
import com.android.to_the_new.application.BaseApplication
import com.android.to_the_new.di.modules.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


@Singleton
@Component(
    modules = [(AndroidSupportInjectionModule::class),
        (AppModule::class),
        (UtilsModules::class),
        (ActivityModule::class),
        (FragmentModule::class),
        (ServiceModule::class),
        (ViewModelModule::class)]
)
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppComponent
    }

    fun inject(instance: BaseApplication)
}