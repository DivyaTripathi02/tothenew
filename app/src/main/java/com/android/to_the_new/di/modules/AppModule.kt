package com.android.to_the_new.di.modules

import android.app.Application
import androidx.room.Room
import com.android.to_the_new.roomDb.AppDatabase
import com.android.to_the_new.roomDb.daos.LocationDao
import com.android.to_the_new.utils.AppConstants
import com.android.to_the_new.utils.SessionManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideMyPref(application: Application): SessionManager = SessionManager(application)


      @Provides
      @Singleton
      @Synchronized
      fun provideDatabase(application: Application): AppDatabase {
          return Room.databaseBuilder(application, AppDatabase::class.java, AppConstants.TO_THE_NEW_DB)
              .fallbackToDestructiveMigration()
              .build()
      }

    @Provides
    @Singleton
    fun provideRoomDao(database: AppDatabase): LocationDao =database.getLocationDao()


}