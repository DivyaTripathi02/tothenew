package com.android.to_the_new.service

import android.annotation.SuppressLint
import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.os.Bundle
import android.os.IBinder
import android.os.Looper
import android.provider.Settings
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.gms.location.*
import dagger.android.AndroidInjection
import java.util.*


class LocationService : Service() {

    private val mFusedLocationClient: FusedLocationProviderClient by lazy {
        LocationServices.getFusedLocationProviderClient(this)
    }


    private var wayLatitude = 0.0
    private var wayLongitude = 0.0
    private var wayAccuracy = 0F


    private val locationRequest: LocationRequest = LocationRequest.create()
    private var locationCallback: LocationCallback? = null

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        AndroidInjection.inject(this)

    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        getLocation()
        startTimer()
        return START_NOT_STICKY
    }


    private fun getLocation() {
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
/*        locationRequest.interval = 20000
        locationRequest.fastestInterval = 20000*/
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                if (locationResult == null) {
                    mFusedLocationClient.removeLocationUpdates(locationCallback)
                    return
                }
                for (location in locationResult.locations) {
                    if (location != null) {
                        wayLatitude = location.latitude
                        wayLongitude = location.longitude
                        wayAccuracy = location.accuracy
                        Log.d("locations_0", "$wayLatitude $wayLongitude $wayAccuracy")
                        val b = Bundle()
                        b.putDouble("wayLatitude", wayLatitude)
                        b.putDouble("wayLongitude", wayLongitude)
                        b.putFloat("wayAccuracy", wayAccuracy)
                        val intent = Intent()
                        intent.putExtras(b)
                        intent.action = "Location"
                        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent)

                    } else checkProviderEnabled()
                }
            }

        }
        requestForLocationUpdates()


    }

    private fun checkProviderEnabled() {
        val lm: LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        var gpsEnabled = false
        var networkEnabled = false

        try {
            gpsEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
        } catch (ex: Exception) {
        }

        try {
            networkEnabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        } catch (ex: Exception) {
        }

        if (!gpsEnabled && !networkEnabled) {
            // notify user
            AlertDialog.Builder(this)
                .setMessage("Your GPS is off, Please enable it so that we can fetch your location")
                .setPositiveButton("Enable GPS") { _, _ ->
                    startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                }
                .setNegativeButton("Cancel") { _, _ -> }
                .show()
        } else requestForLocationUpdates()

    }

    @SuppressLint("MissingPermission")
    fun requestForLocationUpdates() {
        mFusedLocationClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            Looper.getMainLooper()
        )
    }

    private var timer: Timer? = null
    private var timerTask: TimerTask? = null
    private fun startTimer() {
        timer = Timer()
        timerTask = object : TimerTask() {
            override fun run() {
                Log.d("locations_123", "$wayLatitude $wayLongitude $wayAccuracy")
                getLocation()
            }
        }
        timer?.schedule(timerTask, 5000, 5000) //
    }

    private fun stopTimerTask() {
        if (timer != null) {
            timer?.cancel()
            timer = null
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        stopTimerTask()
    }

}