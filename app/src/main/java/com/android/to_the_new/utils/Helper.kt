package com.android.to_the_new.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.provider.Settings
import androidx.appcompat.app.AlertDialog
import java.text.SimpleDateFormat
import java.util.*


fun Context.checkProviderEnabled() {
    val lm: LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
    var gpsEnabled = false
    var networkEnabled = false

    try {
        gpsEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
    } catch (ex: Exception) {
    }

    try {
        networkEnabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
    } catch (ex: Exception) {
    }

    if (!gpsEnabled && !networkEnabled) {
        // notify user
        AlertDialog.Builder(this)
            .setMessage("Your GPS is off, Please enable it so that we can fetch your location")
            .setPositiveButton("Enable GPS") { _, _ ->
                startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
            }
            .setNegativeButton("Cancel") { _, _ -> }
            .show()
    }

}


@SuppressLint("SimpleDateFormat")
 fun getDateFormat(date: Date): String {
    val outputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'z'")
    return outputFormat.format(date)
}

@SuppressLint("SimpleDateFormat")
 fun getStringToDate(date: String): Long {
    val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'z'")
    val objDate = dateFormat.parse(date)
    return objDate.time
}

@SuppressLint("SimpleDateFormat")
 fun getLongToDate(date: Long): String {
    val outputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'z'")
    return outputFormat.format(date)
}
