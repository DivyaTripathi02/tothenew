package com.android.to_the_new.utils

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.android.to_the_new.models.TripInfo
import com.google.gson.Gson
import com.google.gson.JsonParseException
import com.google.gson.JsonSyntaxException
import javax.inject.Inject

class SessionManager @Inject constructor(
    private val application: Application
) {
    private val TRIP_INFO = "trip_info"

    private val sharedPrefs: SharedPreferences =
        application.getSharedPreferences(AppConstants.SHARED_PREF_NAME, Context.MODE_PRIVATE)

    var tripInfo: TripInfo?
        get() = try {
            Gson().fromJson(sharedPrefs.getString(TRIP_INFO, null), TripInfo::class.java)
        } catch (e: JsonParseException) {
            null
        } catch (e: JsonSyntaxException) {
            null
        }
        set(value) = sharedPrefs.edit().putString(TRIP_INFO, Gson().toJson(value)).apply()

}