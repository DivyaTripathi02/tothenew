package com.android.to_the_new.repository

import com.android.to_the_new.models.LocationData
import com.android.to_the_new.roomDb.daos.LocationDao
import kotlinx.coroutines.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LocationRepository @Inject constructor(
    private val dao: LocationDao
) {

    fun addLocation(locationData: LocationData) {
        CoroutineScope(Dispatchers.IO).launch {
            dao.addLocation(locationData)
        }
    }
    suspend fun getLocation(): List<LocationData> = GlobalScope.async {
        return@async dao.getLocation()
    }.await()


    fun deleteLocation() {
        CoroutineScope(Dispatchers.IO).launch {
            dao.deleteLocation()
        }
    }
}
