package com.android.to_the_new.roomDb


import androidx.room.TypeConverter
import com.android.to_the_new.models.LocationData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.Serializable


class LocationXConverter : Serializable {

    @TypeConverter
    fun fromDataToList(list: List<LocationData>): String {
        Gson().also { gson ->
            object : TypeToken<List<LocationData>>() {}.type.also {
                return gson.toJson(list, it)
            }
        }
    }

    @TypeConverter
    fun toListfromString(json: String): List<LocationData> {
        Gson().also { gson ->
            object : TypeToken<List<LocationData>>() {}.type.also {
                return gson.fromJson(json, it)
            }
        }
    }
}