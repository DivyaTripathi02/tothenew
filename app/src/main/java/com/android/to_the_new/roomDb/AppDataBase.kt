package com.android.to_the_new.roomDb

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.android.to_the_new.models.LocationData
import com.android.to_the_new.roomDb.daos.LocationDao


@Database(entities = [LocationData::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun getLocationDao():LocationDao
}
