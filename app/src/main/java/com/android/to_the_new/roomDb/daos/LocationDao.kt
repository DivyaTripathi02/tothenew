package com.android.to_the_new.roomDb.daos

import androidx.room.*
import com.android.to_the_new.models.LocationData

@Dao
interface LocationDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun addLocation(dataItem: LocationData)

    @Query("SELECT * FROM location")
    fun getLocation():List<LocationData>

    @Query("DELETE FROM location")
    fun deleteLocation()}